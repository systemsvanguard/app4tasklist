import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
//pages 
import { NotfoundComponent  } from "./components/site/notfound/notfound.component";
import { AboutComponent  } from "./components/site/about/about.component";
import { TesterComponent  } from "./components/site/tester/tester.component";
import { LoginComponent  } from "./components/site/login/login.component";

import { DashboardComponent  } from "./components/admin/dashboard/dashboard.component";
import { DelegatesComponent  } from "./components/admin/delegates/delegates.component";
import { EngineerlistComponent  } from "./components/admin/engineers/engineerlist/engineerlist.component";
import { EngineerdetailsComponent  } from "./components/admin/engineers/engineerdetails/engineerdetails.component";
import { EngineeraddComponent  } from "./components/admin/engineers/engineeradd/engineeradd.component";
import { LongweekendsComponent  } from "./components/admin/longweekends/longweekends.component";
import { SettingsComponent  } from "./components/admin/settings/settings.component";
import { WeightsComponent  } from "./components/admin/weights/weights.component";
import { AdmintasklistComponent  } from "./components/admin/tasks/admintasklist/admintasklist.component";
import { AdmintaskdetailsComponent  } from "./components/admin/tasks/admintaskdetails/admintaskdetails.component";
import { AdmintaskaddComponent  } from "./components/admin/tasks/admintaskadd/admintaskadd.component";

import { ActivitylogComponent  } from "./components/main/activitylog/activitylog.component";
import { ResourcesComponent  } from "./components/main/resources/resources.component";
import { ManualtasklistComponent  } from "./components/main/manualtasks/manualtasklist/manualtasklist.component";

import { TasklistComponent  } from "./components/tasks/tasklist/tasklist.component";
import { TaskdetailsComponent  } from "./components/tasks/taskdetails/taskdetails.component";
import { TaskcontainerComponent  } from "./components/tasks/taskcontainer/taskcontainer.component";
import { AutomatedtaskcontainerComponent } from "./components/tasks/automatedtaskcontainer/automatedtaskcontainer.component";
import { AutomatedtasklistComponent } from "./components/tasks/automatedtasklist/automatedtasklist.component";
import { AutomatedtaskdetailsComponent } from "./components/tasks/automatedtaskdetails/automatedtaskdetails.component";

const routes: Routes = [
  {path: '', redirectTo: '/tasklist', pathMatch: 'full' },
  {path: 'about', component: AboutComponent }, 
  {path: 'tasklist', component: TasklistComponent },
  {path: 'tasklist/:id', component: TaskdetailsComponent },

  {path: 'autotasks',
    children: [
      { path: 'list', component: AutomatedtasklistComponent},
      { path: ':id',  component: AutomatedtaskdetailsComponent },
    ]
  }, 

  {path: 'tester', component: TesterComponent },
  {path: 'login', component: LoginComponent },
  
  {path: 'dashboard', component: DashboardComponent },
  {path: 'delegates', component: DelegatesComponent },
  {path: 'longweekends', component: LongweekendsComponent },
  {path: 'settings', component: SettingsComponent } ,
  {path: 'weights', component: WeightsComponent } ,
  
  {path: 'activitylog', component: ActivitylogComponent } ,
  {path: 'resources', component: ResourcesComponent } ,
  {path: 'manualtasklist', component: ManualtasklistComponent },
  // {path: 'manualtasklist/:id', component: ManualtasklistComponent }, // ManualtaskdetailComponent
  {path: 'engineers', component: EngineerlistComponent },
  {path: 'engineers/:id', component: EngineerdetailsComponent },
  {path: 'admintasklist', component: AdmintasklistComponent },
  {path: 'admintasklist/:id', component: AdmintaskdetailsComponent },
  
  {path: "**", component: NotfoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
