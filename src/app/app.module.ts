import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule  } from "@angular/common/http";  
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NgxPaginationModule } from "ngx-pagination";
import { Ng2SearchPipeModule } from "ng2-search-filter";
// import { NgxChartsModule } from '@swimlane/ngx-charts';
// pages & components  

import { AppRoutingModule } from './app-routing.module';
import { NotfoundComponent } from './components/site/notfound/notfound.component';
import { AboutComponent } from './components/site/about/about.component';
import { TesterComponent } from './components/site/tester/tester.component';
import { TasklistComponent } from './components/tasks/tasklist/tasklist.component';
import { ResourcesComponent } from './components/main/resources/resources.component';
import { ActivitylogComponent } from './components/main/activitylog/activitylog.component';
import { ManualtasklistComponent } from './components/main/manualtasks/manualtasklist/manualtasklist.component';
import { SitenavbarComponent } from './components/shared/sitenavbar/sitenavbar.component';
import { SitefooterComponent } from './components/shared/sitefooter/sitefooter.component';
import { Etl28dayanalysisComponent } from './components/shared/etl28dayanalysis/etl28dayanalysis.component';
import { BargraphsummaryComponent } from './components/shared/bargraphsummary/bargraphsummary.component';
import { TaskdetailsComponent } from './components/tasks/taskdetails/taskdetails.component';
import { SettingsComponent } from './components/admin/settings/settings.component';
import { DashboardComponent } from './components/admin/dashboard/dashboard.component';
import { DelegatesComponent } from './components/admin/delegates/delegates.component';
import { LongweekendsComponent } from './components/admin/longweekends/longweekends.component';
import { WeightsComponent } from './components/admin/weights/weights.component';
import { AdmintasklistComponent } from './components/admin/tasks/admintasklist/admintasklist.component';
import { AdmintaskdetailsComponent } from './components/admin/tasks/admintaskdetails/admintaskdetails.component';
import { AdmintaskaddComponent } from './components/admin/tasks/admintaskadd/admintaskadd.component';
import { EngineeraddComponent } from './components/admin/engineers/engineeradd/engineeradd.component';
import { EngineerdetailsComponent } from './components/admin/engineers/engineerdetails/engineerdetails.component';
import { EngineerlistComponent } from './components/admin/engineers/engineerlist/engineerlist.component';
import { TaskcontainerComponent } from './components/tasks/taskcontainer/taskcontainer.component';
import { LoginComponent } from './components/site/login/login.component';
import { AutomatedtaskcontainerComponent } from './components/tasks/automatedtaskcontainer/automatedtaskcontainer.component';
import { AutomatedtaskdetailsComponent } from './components/tasks/automatedtaskdetails/automatedtaskdetails.component';
import { AutomatedtasklistComponent } from './components/tasks/automatedtasklist/automatedtasklist.component';

@NgModule({
  declarations: [
    AppComponent,
    NotfoundComponent,
    AboutComponent,
    TesterComponent,
    TasklistComponent,
    ResourcesComponent,
    ActivitylogComponent,
    ManualtasklistComponent,
    SitenavbarComponent,
    SitefooterComponent,
    Etl28dayanalysisComponent,
    BargraphsummaryComponent,
    TaskdetailsComponent,
    SettingsComponent,
    DashboardComponent,
    DelegatesComponent,
    LongweekendsComponent,
    WeightsComponent,
    AdmintasklistComponent,
    AdmintaskdetailsComponent,
    AdmintaskaddComponent,
    EngineeraddComponent,
    EngineerdetailsComponent,
    EngineerlistComponent,
    TaskcontainerComponent,
    LoginComponent,
    AutomatedtaskcontainerComponent,
    AutomatedtaskdetailsComponent,
    AutomatedtasklistComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule, 
    BrowserAnimationsModule, 
    FormsModule, 
    ReactiveFormsModule , 
    BsDatepickerModule.forRoot() ,
    TabsModule.forRoot(), 
    // NgxChartsModule	, 
    NgxPaginationModule , 
    Ng2SearchPipeModule 

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
