import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LongweekendsComponent } from './longweekends.component';

describe('LongweekendsComponent', () => {
  let component: LongweekendsComponent;
  let fixture: ComponentFixture<LongweekendsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LongweekendsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LongweekendsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
