import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmintaskaddComponent } from './admintaskadd.component';

describe('AdmintaskaddComponent', () => {
  let component: AdmintaskaddComponent;
  let fixture: ComponentFixture<AdmintaskaddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdmintaskaddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmintaskaddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
