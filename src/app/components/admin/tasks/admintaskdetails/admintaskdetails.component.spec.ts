import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmintaskdetailsComponent } from './admintaskdetails.component';

describe('AdmintaskdetailsComponent', () => {
  let component: AdmintaskdetailsComponent;
  let fixture: ComponentFixture<AdmintaskdetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdmintaskdetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmintaskdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
