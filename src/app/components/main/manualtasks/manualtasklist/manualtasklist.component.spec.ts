import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManualtasklistComponent } from './manualtasklist.component';

describe('ManualtasklistComponent', () => {
  let component: ManualtasklistComponent;
  let fixture: ComponentFixture<ManualtasklistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManualtasklistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualtasklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
