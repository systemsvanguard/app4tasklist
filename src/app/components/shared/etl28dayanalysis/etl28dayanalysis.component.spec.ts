import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Etl28dayanalysisComponent } from './etl28dayanalysis.component';

describe('Etl28dayanalysisComponent', () => {
  let component: Etl28dayanalysisComponent;
  let fixture: ComponentFixture<Etl28dayanalysisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Etl28dayanalysisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Etl28dayanalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
