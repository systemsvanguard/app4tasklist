import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SitenavbarComponent } from './sitenavbar.component';

describe('SitenavbarComponent', () => {
  let component: SitenavbarComponent;
  let fixture: ComponentFixture<SitenavbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SitenavbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SitenavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
