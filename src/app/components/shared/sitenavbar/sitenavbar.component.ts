import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sitenavbar',
  templateUrl: './sitenavbar.component.html',
  styleUrls: ['./sitenavbar.component.css']
})
export class SitenavbarComponent implements OnInit {

  curentDate: number = Date.now();
  userId = 'LOB';
  userName = 'Errol Lobo';
  
  constructor() { }

  ngOnInit(): void {
  }

}
