import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomatedtaskcontainerComponent } from './automatedtaskcontainer.component';

describe('AutomatedtaskcontainerComponent', () => {
  let component: AutomatedtaskcontainerComponent;
  let fixture: ComponentFixture<AutomatedtaskcontainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutomatedtaskcontainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomatedtaskcontainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
