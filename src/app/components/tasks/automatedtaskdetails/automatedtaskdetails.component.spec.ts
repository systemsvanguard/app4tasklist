import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomatedtaskdetailsComponent } from './automatedtaskdetails.component';

describe('AutomatedtaskdetailsComponent', () => {
  let component: AutomatedtaskdetailsComponent;
  let fixture: ComponentFixture<AutomatedtaskdetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutomatedtaskdetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomatedtaskdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
