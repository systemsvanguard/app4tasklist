import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomatedtasklistComponent } from './automatedtasklist.component';

describe('AutomatedtasklistComponent', () => {
  let component: AutomatedtasklistComponent;
  let fixture: ComponentFixture<AutomatedtasklistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutomatedtasklistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomatedtasklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
