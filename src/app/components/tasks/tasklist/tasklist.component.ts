import { Component, OnInit, Injectable } from '@angular/core';
// import { TasklistService } from "src/app/services/tasklist.service";
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-tasklist',
  templateUrl: './tasklist.component.html',
  styleUrls: ['./tasklist.component.css']
})
export class TasklistComponent implements OnInit {

  lblTitle = 'HH by PAs';
  lblIncomplete = 'Incomplete';
  lblComplete = 'Complete';
  lblFlagged = 'Flagged';

  hhbypas : any;
  tasksbyhhs : any;

  //----------------->
  private current_pa    = 'lob';
  private current_date  = '2021-11-22';
  private current_hh    = '3701660';

  public hhbypas_incompl : any ; // hhbypa ~ HouseHold by PA 
  public hhbypas_compl : any ; 
  public hhbypas_flag : any ; 
  public tasksbyhh : any;  // Tasks by Household 
  //----------------->

  //------ pagination & search for household list ------>
  // pagination package : https://www.npmjs.com/package/ngx-pagination 
  p: number = 1;  // pagination page number 
  // search filter package "ng2-search-filter": https://www.npmjs.com/package/ng2-search-filter  
  searchterm = '';

  
  constructor(private http: HttpClient) { }


  ngOnInit() {
      this.getHHbyPA_incomplete() 
    , this.getHHbyPA_complete() 
    , this.getHHbyPA_flag() 
    , this.getTasksbyHH()  
  }

  //------------------------> 
  getTasksbyHH() {  // Tasks by Household 
    // const current_pa = 'lob';
    const apiTasksbyHH = `http://localhost:1339/api/household/${this.current_pa}/${this.current_hh}/${this.current_date}/` ; 
    this.http.get<any>(apiTasksbyHH).subscribe((res) => {
      this.tasksbyhh  =  res.tasks;  
      console.log(this.tasksbyhh)
    }  )
  }


  getHHbyPA_incomplete() { // Households by PA with Incomplete Tasks 
    const apiHHbyPA_incompl = `http://localhost:1339/api/engtasks/${this.current_pa}/${this.current_date}/` ; 
    // const apiHHbyPA ='https://tasklistapijson.openode.io/LOB/' ; 
    this.http.get<any>(apiHHbyPA_incompl).subscribe((res) => {
      this.hhbypas_incompl  =  res.incomplete  
      console.log(this.hhbypas_incompl)
    }  )
  }

  getHHbyPA_complete() {  // Households by PA with Completed Tasks 
    const apiHHbyPA_compl = `http://localhost:1339/api/engtasks/${this.current_pa}/${this.current_date}/` ; 
    this.http.get<any>(apiHHbyPA_compl).subscribe((res) => {
      this.hhbypas_compl  =  res.completed  
      console.log(this.hhbypas_compl) 
    }  )
  }

  getHHbyPA_flag() {   // Households by PA with Flagged Tasks 
    const apiHHbyPA_flag = `http://localhost:1339/api/engtasks/${this.current_pa}/${this.current_date}/` ; 
    this.http.get<any>(apiHHbyPA_flag).subscribe((res) => {
      this.hhbypas_flag  =  res.flagged  
      console.log(this.hhbypas_flag)
    }  )
  }
  //------------------------>  




}
