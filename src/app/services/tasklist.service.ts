import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';



@Injectable({
  providedIn: 'root'
})

export class TasklistService {


  // private apiURL = 'https://my-json-server.typicode.com/JSGund/XHR-Fetch-Request-JavaScript/posts';
  // private apiURL = 'http://localhost:1339/api/engtasks';

  private current_pa = 'lob';
  private current_date    = '2021-11-22';
  private my_date    = '2021-11-22';
  private apiHHbyPA    = `https://tasklistapijson.openode.io/${this.current_pa}/`;
  // private apiHHbyPA    = `http://localhost:1339/api/engtasks/${this.current_pa}/${this.current_date}/`;
  
  private apiTasksbyPA = `https://tasklistapijson.openode.io/tasksbyhh`;
  private apiURL     = `https://tasklistapijson.openode.io/${this.current_pa}/`;
  // private apiURL = `https://tasklistapijson.openode.io/${this.current_pa}/`;
  // private apiURL = `https://tasklistapijson.openode.io/LOB/`;
  
  public hhbypas : any ;  
  public hhbypas_incompl : any ; // hhbypa ~ HouseHold by PA 
  public hhbypas_compl : any ; 
  public hhbypas_flag : any ; 
  public tasksbyhh : any;  // Tasks by Household 


  constructor(private http: HttpClient) { }


  //--------------------->
  getHHbyPA() {
    
    return this.http.get(this.apiHHbyPA);
  }


  getTasksbyPA() {
    
    return this.http.get(this.apiTasksbyPA);
  }

  //----------------------> 

  // Incomplete Tasks - households by PA
  getHHbyPA_incomplete() {
    const apiHHbyPA =`http://localhost:1339/api/engtasks/${this.current_pa}/${this.current_date}/` ; 
    // const apiHHbyPA =`http://localhost:1339/api/engtasks/lob/2021-11-22/` ; 
    // const apiHHbyPA ='https://tasklistapijson.openode.io/LOB/' ; 
    this.http.get<any>(apiHHbyPA).subscribe((res) => {
      this.hhbypas_incompl  =  res.incomplete  
      console.log(this.hhbypas_incompl)
    }  )
  }

  getHHbyPA_complete() {
    const apiHHbyPA ='http://localhost:1339/api/engtasks/lob/2021-11-22/' ; 
    this.http.get<any>(apiHHbyPA).subscribe((res) => {
      this.hhbypas_compl  =  res.completed  
      console.log(this.hhbypas_compl)
    }  )
  }

  // Flagged Tasks - households by PA
  getHHbyPA_flag() {
    const apiHHbyPA ='http://localhost:1339/api/engtasks/lob/2021-11-22/' ; 
    this.http.get<any>(apiHHbyPA).subscribe((res) => {
      this.hhbypas_flag  =  res.flagged  
      console.log(this.hhbypas_flag)
    }  )
  }
  
  //----------------------------->

}
